# meteo

This Python 3 program prepares meteorological data for further evaluation with tools such as SPSS.

## Usage: python3 meteo.py

- Example screencast [please use high video quality [settings 1080p](https://support.google.com/youtube/answer/91449?hl=en)]:

[![Video example of using meteo.py to prepare CSV data for SPSS](http://img.youtube.com/vi/oGWgoCr1g_Q/0.jpg)](https://www.youtube.com/embed/oGWgoCr1g_Q)

https://youtu.be/oGWgoCr1g_Q

<small>Thanks [Markdown](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/)!</small>