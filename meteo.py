#!/usr/bin/python3

# This processes .CSV data from meteorological stations to 6 columns -> DataType, MeteoStation, Year, Month, Day, Value.
# 2020 jk

# Run in command-line with: 
# python3 meteo.py

import csv
import xlrd
import array
import sys, os

# LOOP over input .CSV files in given directory. 

# (Testing) Get directory from script current location.
# On Windows: directory = os.path.join("c:\\","path")
# On Linux: absFilePath = os.path.abspath(__file__)
	# path, filename = os.path.split(absFilePath)

# PLEASE edit directory path with input .csv files manually!
directory = "/home/kali/Downloads/meteo/input_csv_maxteplo/"
for root,dirs,files in os.walk(directory):
	for input_file in files:
		if input_file.endswith(".csv"): # Example: input_file "C1KOCE01.csv"
			output_file="spss_maxteplo_"+input_file

		# READ input .csv, process rows and output prepared data to console.
		with open(os.path.join(directory,input_file), "r", newline='') as infile,  open(output_file, "w", newline='') as outfile:
			reader = csv.reader(infile, delimiter=';')
			# HEADERS - Skip the headers when reading.
			headers_meteo = next(reader, None) 
			# We will return headers (or None if input file is empty) later.
			headers_station = next(reader, None)
			next(reader, None) # Skip blankline & some original column descriptions.
			next(reader, None) 
			writer = csv.writer(outfile, delimiter=';')
			if headers_meteo:
				meteo = headers_meteo[0].split(" ") # Describes type of meteorogical data.
				datatype = meteo[0]+meteo[2] # For example: Max. teplota.
			if headers_station:
				station = headers_station[0].split(" ") # Station name is in station[1].
			# We create own column descriptions.
			writer.writerow(["typdat","stanice","rok","mesic","den","hodnota"]) 
			# Process each row of input file.
			for row in reader:
				# Hint:row is an array of 33 fields with values from line in input file!
				for day in list(range(2,len(row))):
					# Write data arranged in 4 columns to output file. Skip if meteo value empty.
					if row[day] != "":
						writer.writerow([datatype,station[1],row[0],row[1],day-1,row[day]])
		# One file finished.
		print('Done processing file "{0}" for SPSS -> Results saved to file: {1}'.format(input_file, output_file))

# TODO: Convert .xls sheet to .csv. Ask user for path with input .csv files. Validate input. Feedback.
# DEBUG: Same output with arragned data can be printed to console only.
#		  print('{0};{1};{2};{3};{4};{5}'.format(datatype,station[1],row[0],row[1],day-1,row[day]))
# HINTS:
# https://stackoverflow.com/questions/9884353/xls-to-csv-converter/9884551#9884551
# https://stackoverflow.com/questions/33503993/read-in-all-csv-files-from-a-directory-using-python/33504072#33504072
# https://stackoverflow.com/questions/14257373/skip-the-headers-when-editing-a-csv-file-using-python/14257599#14257599